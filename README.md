# # Dados


```python
2  MES
6  UF
7  NOME_DO_MUNICIPIO
15 FABRICANTE
16 MODELO
21 MEMORIA_TOTAL
22 MEMORIA_LIVRE
23 USO_PROCESSADOR
25 VERSÃO_SISTEMA_OPERACIONAL
27 NIVEL_DA_BATERIA
28 ASN
29 IP_PUBLICO
30 MAC_ADDRESS
31 SSID
32 BSSID
34 BANDA_FREGUENCIA
35 NIVEL_SINAL
37 SERVIDOR_DE_MEDICAO
38 DATA_HORA_LOCAL
43 DOWLOAD_KBPS
44 UPLOAD KBPS
45 LATENCIA_MS
46 JITTER_MS
54 CM_IND4_UL
55 CM_IND4_DL
56 CM_IND5
57 CM IND6
58 CM IND7
```

## Exemplo limpeza e indexação

cat  BASE_PROCESSADA_SCM_IND4aIND7_0*2022_operadora.csv  | cut -d ';' -f 2,6,15,16,21,22,23,25,27,28,29,30,31,32,34,35,37,38,43,44,45,46,54,55,56,57,58 >esac_data.csv

##  Analise exploratória, veja o seu Encoding antes....


```python
import pandas as pd
import sweetviz as sv
esac_data = pd.read_csv(r"C:\Users\teste\Downloads\FTP\esac_data.csv",sep=';',encoding='ANSI',low_memory=False)
#print(esac_data) 
my_report = sv.analyze(esac_data)
my_report.show_html() 
#print(esac_data.head())""
```

![Exemplo](https://gitlab.com/gt3.operadoras/analisedados/-/raw/main/files/exemplo.PNG)